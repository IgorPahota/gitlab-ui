import description from './segmented_control.md';

export default {
  followsDesignSystem: false,
  description,
};
